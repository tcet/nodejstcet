const express = require("express");
const port = 8080;

const app = express();
app.use(express.json());

var cors = require('cors');
app.use(cors());


app.get("/", (req, res) => {
	res.send("Hello Friends Chai Pilo..");
});

app.get("/sqr/:num", (req, res) => {
	num = req.params.num
	console.log(req.body);
	res.send(`Square of ${num} is ${num * num}`);
});

app.get("/calc/", (req, res) => {
	x = req.body.num1;
	y = req.body.num2;
	res.send(`${x} + ${y} = ${x + y}`);
});

app.post("/calc/", (req, res) => {
	x = req.body.num1;
	y = req.body.num2;
	res.send(`${x} - ${y} = ${x - y}`);
});

app.put("/calc/", (req, res) => {
	x = req.body.num1;
	y = req.body.num2;
	res.send(`${x} * ${y} = ${x * y}`);
});

app.delete("/calc/", (req, res) => {
	x = req.body.num1;
	y = req.body.num2;
	res.send(`${x} / ${y} = ${x / y}`);
});

//app.post();

/*
Create	-	POST	-	Insert
Read 	-	GET		-	Select/Find
Update	- 	PUT		-	Update
Delete 	- 	DELETE	- 	Delete
*/


app.listen(process.env.PORT || port, () => {	
	console.log(`Listening on port ${port}`);
});













/*
fun = require("./function");

console.log(fun.add(5, 6));
console.log(fun.sub(5, 6));
*/